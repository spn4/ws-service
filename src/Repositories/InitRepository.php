<?php

namespace SpondonIt\WsService\Repositories;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use App\Notification;
use Modules\Localization\Entities\Language;
use Modules\RolePermission\Entities\Role;
use Modules\Setting\Model\BusinessSetting;
use Modules\Setting\Model\GeneralSetting;
use Nwidart\Modules\Facades\Module;

class InitRepository
{

    public function init()
    {
        config([
            'app.item' => '32806276',
            'spondonit.module_manager_model' => \Modules\ModuleManager\Entities\InfixModuleManager::class,
            'spondonit.module_manager_table' => 'infix_module_managers',

            'spondonit.settings_model' => GeneralSetting::class,
            'spondonit.module_model' => Module::class,

            'spondonit.user_model' => \App\Models\User::class,
            'spondonit.settings_table' => 'general_settings',
            'spondonit.database_file' => 'wchat.sql',
            'spondonit.verifier' => 'auth',
            'spondonit.support_multi_connection' => false,
            'spondonit.saas_module_name' => 'WsSaas',
            'spondonit.module_status_check_function' => 'isModuleActive',
        ]);
    }


    public function config(){

        if (Schema::hasTable('languages')){
            $directories = array_map('basename', \File::directories(resource_path('lang')));
            $languages = Language::whereIn('code', $directories)->get();
        }else{
            $languages = [];
        }


        view()->composer('backend.partials._menu', function ($view) use ($languages) {
            $view->with('languages', $languages);
        });

    }

}
