<?php

namespace SpondonIt\WsService;

use Illuminate\Support\ServiceProvider;
use Illuminate\Contracts\Http\Kernel;
use SpondonIt\WsService\Middleware\WsService;

class SpondonItWsServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $kernel = $this->app->make(Kernel::class);
        $kernel->pushMiddleware(WsService::class);

        $this->loadRoutesFrom(__DIR__.'/../routes/web.php');
        $this->loadTranslationsFrom(__DIR__.'/../resources/lang', 'ws');
        $this->loadViewsFrom(__DIR__.'/../resources/views', 'ws');
        $this->loadMigrationsFrom(__DIR__.'/../database/migrations');
    }
}
